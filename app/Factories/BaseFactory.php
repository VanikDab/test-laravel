<?php


namespace App\Factories;


 use App\Models\BaseObject;
 use Illuminate\Support\Facades\Config;

 abstract class BaseFactory
{

     protected $data;

     protected $type;

     /**
      * BaseFactory constructor.
      * @param BaseObject $data
      */
     public function __construct(BaseObject $data)
     {
         $this->data = $data;
         $this->type = Config::get('savedata.default');;
     }

     /**
      * @param $type
      */
    public  abstract function run($type = null);

}
