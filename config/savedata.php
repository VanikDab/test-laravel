<?php

return [
    'default' => 'Json',

    'types' => [
        'database' => 'Db',
        'json' => 'Json',
        'email' => 'Email',
    ]
];
